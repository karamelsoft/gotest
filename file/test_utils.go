package file

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

type LoadTest struct {
	Name     string
	Supplier func() Loader
	FileName string
	Want     interface{}
	WantErr  bool
}

type SaveTest struct {
	Name     string
	Supplier func() Saver
	Value    interface{}
	FileName string
}

type CompareTest struct {
	Name     string
	Supplier func() Comparator
	FileName string
	WantErr  bool
}

func Load(t *testing.T, tt LoadTest) {
	input, err := os.Open(InputFile(tt.FileName))
	if err != nil && !tt.WantErr {
		t.Errorf("could not open file %s : %v", err, tt.WantErr)
		return
	}
	defer input.Close()

	got, err := tt.Supplier().Load(input)
	if (err != nil) != tt.WantErr {
		t.Errorf("could load value : %v", err)
		return
	}
	if !reflect.DeepEqual(got, tt.Want) {
		t.Errorf("got = %v, want %v", got, tt.Want)
	}
}

func Save(t *testing.T, tt SaveTest) {
	output, err := os.Create(OutputFile(tt.FileName))
	if err != nil {
		t.Errorf("could not create file %s : %v", tt.FileName, err)
		return
	}
	defer output.Close()

	err = tt.Supplier().Save(tt.Value, output)
	if err != nil {
		t.Errorf("could save value %v : %v", tt.Value, err)
	}
}

func Compare(t *testing.T, tt CompareTest) {
	inputPath := InputFile(tt.FileName)
	input, err := os.Open(inputPath)
	if err != nil && !tt.WantErr {
		t.Errorf("could not open file %s : %v", inputPath, err)
		return
	} else {
		defer input.Close()
	}

	outputPath := OutputFile(tt.FileName)
	output, err := os.Open(outputPath)
	if err != nil && !tt.WantErr {
		t.Errorf("could not open file %s : %v", outputPath, err)
		return
	} else {
		defer output.Close()
	}

	err = tt.Supplier().Compare(output, input)
	if (err != nil) != tt.WantErr {
		t.Errorf("files %s and %s are different", inputPath, outputPath)
		return
	}
}

func InputFile(name string) string {
	return filepath.Join(".test/input", name)
}

func OutputFile(name string) string {
	return filepath.Join(".test/output", name)
}
