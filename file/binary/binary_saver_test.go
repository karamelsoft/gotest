package binary

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestSave(t *testing.T) {

	tests := []file.SaveTest{
		{Name: "content", Supplier: binarySaver(), Value: []byte("Robert Patoulatchy"), FileName: "new_person"},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Save(t, tt)
		})
	}
}

func binarySaver() func() file.Saver {
	return NewBinarySaver
}
