package binary

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestCompare(t *testing.T) {

	tests := []file.CompareTest{
		{Name: "same existing file", Supplier: supplier(), FileName: "person", WantErr: false},
		{Name: "different existing file", Supplier: supplier(), FileName: "wrong", WantErr: true},
		{Name: "non existing file", Supplier: supplier(), FileName: "doesnotexist", WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Compare(t, tt)
		})
	}
}

func supplier() func() file.Comparator {
	return NewBinaryComparator
}
