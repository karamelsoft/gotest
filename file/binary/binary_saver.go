package binary

import (
	"gitlab.com/karamelsoft/gotest/file"
	"io"
)

type saver struct{}

func NewBinarySaver() file.Saver {
	return &saver{}
}

func (s saver) Save(value interface{}, writer io.Writer) error {
	_, err := writer.Write(value.([]byte))

	return err
}
