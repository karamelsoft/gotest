package binary

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestLoad(t *testing.T) {

	tests := []file.LoadTest{
		{Name: "existing file", Supplier: binaryLoader(), FileName: "person", Want: []byte("Frederic Gendebien"), WantErr: false},
		{Name: "non existing file", Supplier: binaryLoader(), FileName: "doesnotexist", Want: []byte{}, WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Load(t, tt)
		})
	}
}

func binaryLoader() func() file.Loader {
	return NewBinaryLoader
}
