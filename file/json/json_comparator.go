package json

import (
	"errors"
	"github.com/nsf/jsondiff"
	"gitlab.com/karamelsoft/gotest/file"
	"io"
	"io/ioutil"
)

type ComparisonMode uint32

const (
	Strict ComparisonMode = iota
	Lenient
)

type comparator struct {
	mode ComparisonMode
}

func NewJsonComparator(mode ComparisonMode) file.Comparator {
	return &comparator{
		mode: mode,
	}
}

func (c comparator) Compare(actual, expected io.Reader) error {
	actualBytes, err := ioutil.ReadAll(actual)
	if err != nil {
		return err
	}

	expectedBytes, err := ioutil.ReadAll(expected)
	if err != nil {
		return err
	}

	options := jsondiff.DefaultJSONOptions()
	diff, desc := jsondiff.Compare(
		actualBytes,
		expectedBytes,
		&options,
	)

	if diff == expectedDifferenceFrom(c.mode) {
		return nil
	}

	return errors.New(desc)
}

func expectedDifferenceFrom(mode ComparisonMode) jsondiff.Difference {
	switch mode {
	case Lenient:
		return jsondiff.SupersetMatch
	case Strict:
		return jsondiff.FullMatch
	default:
		return jsondiff.NoMatch
	}
}
