package json

import (
	"encoding/json"
	"gitlab.com/karamelsoft/gotest/file"
	"io"
)

type loader struct {
	value interface{}
}

func NewJsonLoader(value interface{}) file.Loader {
	return &loader{
		value: value,
	}
}

func (j loader) Load(reader io.Reader) (interface{}, error) {
	return j.value, json.NewDecoder(reader).Decode(j.value)
}
