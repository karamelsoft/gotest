package json

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestLoad(t *testing.T) {

	tests := []file.LoadTest{
		{Name: "single map", Supplier: jsonLoader(createEmptyMap()), FileName: "person.json", Want: createExpectedMap(), WantErr: false},
		{Name: "array of maps", Supplier: jsonLoader(createEmptyArrayOfMaps()), FileName: "persons.json", Want: createExpectedArrayOfMaps(), WantErr: false},
		{Name: "single struct", Supplier: jsonLoader(createEmptyStruct()), FileName: "person.json", Want: createExpectedStruct(), WantErr: false},
		{Name: "array of structs", Supplier: jsonLoader(createEmptyArrayOfStructs()), FileName: "persons.json", Want: createExpectedArrayOfStructs(), WantErr: false},
		{Name: "non existing file", Supplier: jsonLoader(createEmptyStruct()), FileName: "doesnotexist.json", Want: &person{}, WantErr: true},
		{Name: "wrong file", Supplier: jsonLoader(createEmptyStruct()), FileName: "wrong.json", Want: &person{}, WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Load(t, tt)
		})
	}
}

func jsonLoader(value interface{}) func() file.Loader {
	return func() file.Loader {
		return NewJsonLoader(value)
	}
}
