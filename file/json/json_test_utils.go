package json

const (
	FirstName1 = "Robert"
	LastName1  = "Patoulatchy"
	FirstName2 = "Roberta"
	LastName2  = "Patoulatcha"
)

type person struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func createEmptyMap() interface{} {
	var m map[string]string
	m = make(map[string]string)

	return &m
}

func createEmptyArrayOfMaps() *[]*map[string]string {
	a := make([]*map[string]string, 0, 2)
	return &a
}

func createExpectedMap() *map[string]string {
	robertMap := make(map[string]string)
	robertMap["firstName"] = FirstName1
	robertMap["lastName"] = LastName1

	return &robertMap
}

func createExpectedArrayOfMaps() *[]*map[string]string {
	robertaMap := make(map[string]string)
	robertaMap["firstName"] = FirstName2
	robertaMap["lastName"] = LastName2

	a := []*map[string]string{
		createExpectedMap(),
		&robertaMap,
	}

	return &a
}

func createEmptyStruct() *person {
	return &person{}
}

func createEmptyArrayOfStructs() *[]*person {
	a := make([]*person, 0, 2)
	return &a
}

func createExpectedStruct() *person {
	return &person{
		FirstName: FirstName1,
		LastName:  LastName1,
	}
}

func createExpectedArrayOfStructs() *[]*person {
	a := []*person{
		createExpectedStruct(),
		&person{
			FirstName: FirstName2,
			LastName:  LastName2,
		},
	}

	return &a
}
