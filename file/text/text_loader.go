package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"gitlab.com/karamelsoft/gotest/file/binary"
	"io"
)

type loader struct{}

func NewTextLoader() file.Loader {
	return &loader{}
}

func (loader loader) Load(reader io.Reader) (interface{}, error) {
	bytes, err := binary.NewBinaryLoader().Load(reader)
	if err != nil {
		return nil, err
	}

	return string(bytes.([]byte)), nil
}
