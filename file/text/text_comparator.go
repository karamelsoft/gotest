package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"gitlab.com/karamelsoft/gotest/file/binary"
)

func NewTextComparator() file.Comparator {
	return binary.NewBinaryComparator()
}
