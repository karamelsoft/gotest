package text

import (
	"gitlab.com/karamelsoft/gotest/file"
	"testing"
)

func TestCompare(t *testing.T) {

	tests := []file.CompareTest{
		{Name: "same existing file", Supplier: textComparator(), FileName: "person.txt", WantErr: false},
		{Name: "different existing file", Supplier: textComparator(), FileName: "wrong.txt", WantErr: true},
		{Name: "non existing file", Supplier: textComparator(), FileName: "doesnotexist.txt", WantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			file.Compare(t, tt)
		})
	}
}

func textComparator() func() file.Comparator {
	return NewTextComparator
}
