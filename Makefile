VERSION="v0.2.0"

deps:
	go mod download
	go mod tidy

build:
	go build -v ./...

test:
	go test -v ./... -cover

release:
	git tag -a $(VERSION) -m "Release $(VERSION)"
	git push origin $(VERSION)
